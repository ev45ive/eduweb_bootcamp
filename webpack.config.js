var HtmlWebpackPlugin = require('html-webpack-plugin')


module.exports = {
    entry:[
        './src/index.js'
    ],
    output:{
        path: __dirname + '/dist/',
        filename: 'bundle.js'
    },
    plugins:[
        new HtmlWebpackPlugin({
            template: './src/index.tpl.html'
        })
    ],
    module:{
        loaders:[
            { test: /\.js$/, loader: 'babel' },
            { test: /\.html$/, loader: 'html' }
        ]
    }
}